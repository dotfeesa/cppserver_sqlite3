/*!
    \file pingpong_tcp_server.cpp
    \brief TCP pingpong server example
    University of Sarajevo
    Department of Telecommunications
    Software Design of Protocols
    www.tk.etf.unsa.ba
    \author Doc.dr Miralem Mehic dipl.ing.el. <miralem.mehic@etf.unsa.ba>
    \date 22.12.2021
    \copyright MIT License
*/

#include "asio_service.h"
#include "server/asio/tcp_server.h"

//https://github.com/d99kris/rapidcsv
//#include "server/rapidcsv/rapidcsv.h"

#include <iostream>
#include "server/json/json.h"

#include <sqlite3.h>
#include "server/sqlite3/sqlite3_wrapper.h"

#include <vector>
#include <iostream>

namespace sqlite = sqlite3_wrapper;
using namespace std;

class PingPongSession : public CppServer::Asio::TCPSession
{
public:
    using CppServer::Asio::TCPSession::TCPSession;

    void setCounterId(int counter){
        m_counterId = counter;
    }

protected:
    void onConnected() override
    {
        std::string cip = socket().remote_endpoint().address().to_string();
        std::cout << "PingPong TCP session with Id " << id() << " (" << cip << ") is connected!" << std::endl;
        std::cout << "Number of connected users is: " << server()->connected_sessions() << std::endl;

        std::string msg = "Welcome to ETF SDP PINGPONG APP!";
        SendMessage(msg);
    }

    void onDisconnected() override
    {
        std::cout << "PingPong TCP session with Id " << id() << " disconnected!" << std::endl;
    }

    nlohmann::json findUserByUsername(std::string usernameParam)
    {
        nlohmann::json output;
        output["VALID"] = 0;
        try
        {
            sqlite::db db("pingpong.db");
            auto select_statement = db.prepare(R"(
                SELECT USERNAME, IP, SID
                FROM USERS
                WHERE (username = ?)
            )");
            select_statement.execute(usernameParam);

            std::string username;
            std::string ip;
            std::string sid;
            if (select_statement.fetch(username, ip, sid))
            {
                output["USERNAME"] = username;
                output["IP"] = ip;
                output["SID"] = sid;
                output["VALID"] = 1;
            }

        }catch (const sqlite::exception &e)
        {
            std::cout << "ERROR READING DATA FROMD DATABASE!" << std::endl;
        }
        return output;
    }

    void onReceived(const void* buffer, size_t size) override
    {
        std::string payload((const char*)buffer, size);
        nlohmann::json msgContent = nlohmann::json::parse(payload);

        std::cout << "Incoming: " << payload << std::endl;

        if(msgContent["MESSAGE_TYPE"] == 0 && msgContent["MESSAGE"] == "REGISTER"){

            nlohmann::json userDetails = findUserByUsername(msgContent["LOCAL"]);

            //user was NOT found
            if(userDetails["VALID"] == 0) {

                std::string local = msgContent["LOCAL"];
                std::string ipAddress = socket().remote_endpoint().address().to_string();
                std::string sessionID = id().string();

                sqlite::db db("pingpong.db");
                auto insert_statement = db.prepare(R"(
                    INSERT INTO USERS(USERNAME, IP, SID)
                    VALUES (?, ?, ?)
                )");

                db.begin();
                insert_statement.execute(
                    local,
                    ipAddress,
                    sessionID
                    );
                db.commit();
            }

            std::string msg = std::string(msgContent["LOCAL"]) + " is now registered!";
            SendMessage(msg);

        }else if(msgContent["MESSAGE_TYPE"] > 0 && msgContent["LOCAL"] != "" && msgContent["PEER"] == ""){

            std::string msg = "Use # to define username of the destination client! ";
            SendMessage(msg);

        }else if(msgContent["LOCAL"] != "" && msgContent["PEER"] != ""){

            nlohmann::json userDetails = findUserByUsername(msgContent["PEER"]);

            //user was NOT found
            if(userDetails["VALID"] == 1)
            {
                // succeeded
                CppCommon::UUID destinationSessionId(userDetails["SID"]);
                std::shared_ptr<TCPSession> peerSession = server()->FindSession(destinationSessionId);

                if(peerSession == nullptr){
                    std::cout << "No client with session id " << userDetails["SID"] << " registered!" << std::endl;
                }else{
                    //we found the destination user;
                    //forward the incoming message to destination
                    if(peerSession->IsConnected() == true){
 
                        if(msgContent["MESSAGE_TYPE"] == 0 && msgContent["MESSAGE"] == "HELLO"){

                            nlohmann::json msgBody;
                            msgBody["PEER"] = msgContent["PEER"];
                            msgBody["LOCAL"] = msgContent["LOCAL"];
                            msgBody["MESSAGE_TYPE"] = 1; //0-signaling communication; 1-unicast
                            msgBody["MESSAGE"] = std::string(msgContent["LOCAL"]) + " established connection with you!";
                            std::string message = msgBody.dump();
                            peerSession->SendAsync(message);

                            //inform user that the message was forwarded
                            std::string msg = "Connected to " + std::string(msgContent["PEER"]) + "!";
                            SendMessage(msg);                            

                        }else{
                            //forward message to peer
                            peerSession->SendAsync(payload);

                            //inform user that the message was forwarded
                            std::string msg = "Message to " + std::string(msgContent["PEER"]) + " was sucessfully sent!";
                            SendMessage(msg);
                        }


                    }else{
                        std::cout << "Client with username " << msgContent["PEER"] << " is disconnected! ";
                    }
                }
            }else{
                std::string msg = "Client with username " + std::string(msgContent["PEER"]) + " is NOT registered! ";
                SendMessage(msg);
            }
        }

        // If the buffer starts with '!' the disconnect the current session
        if (msgContent["MESSAGE"] == "!") Disconnect();
    }

    void onError(int error, const std::string& category, const std::string& message) override
    {
        std::cout << "PingPong TCP session caught an error with code " << error << " and category '" << category << "': " << message << std::endl;
    }

private:

    void SendMessage(std::string msg){
        nlohmann::json msgBody;
        msgBody["MESSAGE_TYPE"] = 0; //0-signaling communication; 1-unicast
        msgBody["MESSAGE"] = msg;
        std::string message = msgBody.dump();
        SendAsync(message);
    }

    int m_counterId;
};


class PingPongServer : public CppServer::Asio::TCPServer
{
public:
    using CppServer::Asio::TCPServer::TCPServer;

protected:
    std::shared_ptr<CppServer::Asio::TCPSession> CreateSession(const std::shared_ptr<CppServer::Asio::TCPServer>& server) override
    {
        std::shared_ptr<CppServer::Asio::TCPSession> session = std::make_shared<PingPongSession>(server);
        return session;
    }
    void onError(int error, const std::string& category, const std::string& message) override
    {
        std::cout << "PingPong TCP server caught an error with code " << error << " and category '" << category << "': " << message << std::endl;
    }
    void onConnected(std::shared_ptr<CppServer::Asio::TCPSession>& session) override
    {
        std::cout << "PingPong TCP server is connected via session " << session->id().string() << std::endl;
    }

public:

    void initCounter(){
        m_counter = 0;
    }

    void sendMulticastMessage(std::string msgText){
        nlohmann::json msgBody;
        msgBody["MESSAGE"] = msgText;
        std::string message = msgBody.dump();
        Multicast(message);
    }

    void createDatabaseStructure()
    {
        try{
            sqlite::db db("pingpong.db");

            auto sql1 = db.prepare(R"(DROP TABLE IF EXISTS USERS)");
            sql1.execute();

            auto sql = db.prepare(R"(
               CREATE TABLE USERS (
                USERNAME         TEXT    NOT NULL,
                IP               TEXT    NOT NULL,
                SID              TEXT    NOT NULL)
            )");
            sql.execute();
        }catch (const sqlite::exception &e)
        {
            std::cout << "ERROR CREATING DATABASE STRUCTURE!" << std::endl;
            std::cerr << e.what() << std::endl;
        }
    }
private:
    int m_counter;
};


int main(int argc, char** argv)
{

    // TCP server port
    int port = 1111;
    if (argc > 1)
        port = std::atoi(argv[1]);

    std::cout << "TCP server port: " << port << std::endl;

    std::cout << std::endl;

    // Create a new Asio service
    auto service = std::make_shared<AsioService>();

    // Start the Asio service
    std::cout << "Asio service starting...";
    service->Start();
    std::cout << "Done!" << std::endl;

    // Create a new TCP PingPong server
    auto server = std::make_shared<PingPongServer>(service, port);
    server->initCounter();
    server->createDatabaseStructure();

    // Start the server
    std::cout << "Server starting...";
    server->Start();
    std::cout << "Done!" << std::endl;

    std::cout << "Press Enter to stop the server or '!' to restart the server..." << std::endl;

    // Perform text input
    std::string line;
    while (getline(std::cin, line))
    {
        if (line.empty())
            break;

        // Restart the server
        if (line == "!")
        {
            std::cout << "Server restarting...";
            server->Restart();
            std::cout << "Done!" << std::endl;
            continue;
        }

        // Multicast admin message to all sessions
        line = "(admin) " + line;
        server->sendMulticastMessage(line);
    }

    // Stop the server
    std::cout << "Server stopping...";
    server->Stop();
    std::cout << "Done!" << std::endl;

    // Stop the Asio service
    std::cout << "Asio service stopping...";
    service->Stop();
    std::cout << "Done!" << std::endl;

    return 0;
}

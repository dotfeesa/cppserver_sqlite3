/*!
    \file pingpong_tcp_client.cpp
    \brief TCP pingpong client example
    University of Sarajevo
    Department of Telecommunications
    Software Design of Protocols
    www.tk.etf.unsa.ba
    \author Doc.dr Miralem Mehic <miralem.mehic@etf.unsa.ba>
    \date 22.12.2021
    \copyright MIT License
*/

#include "asio_service.h"

#include "server/asio/tcp_client.h"
#include "threads/thread.h"

#include "server/json/json.h"

#include <atomic>
#include <iostream>

class PingPongClient : public CppServer::Asio::TCPClient
{
public:
    using CppServer::Asio::TCPClient::TCPClient;

    void DisconnectAndStop()
    {
        _stop = true;
        DisconnectAsync();
        while (IsConnected())
            CppCommon::Thread::Yield();
    }

    void SendMessage(std::string msg, int msgType = 1){

        nlohmann::json msgBody;
        msgBody["PEER"] = m_destination;
        msgBody["LOCAL"] = m_username;
        msgBody["MESSAGE_TYPE"] = msgType; //0-signaling communication; 1-unicast
        msgBody["MESSAGE"] = msg;
        std::string message = msgBody.dump();

        // Send the entered text to the PingPong server
        SendAsync(message);
    }

    void SetDestination(std::string value){
        m_destination = value;
    }
    void SetUsername(std::string value){
        m_username = value;
    }

protected:
    void onConnected() override
    {
        std::cout << "PingPong TCP client (" << m_username << ") connected a new session with Id " << id() << std::endl;
        SendMessage("REGISTER", 0);
    }

    void onDisconnected() override
    {
        std::cout << "PingPong TCP client (" << m_username << ") disconnected a session with Id " << id() << std::endl;

        // Wait for a while...
        CppCommon::Thread::Sleep(1000);

        // Try to connect again
        if (!_stop)
            ConnectAsync();
    }

    void onReceived(const void* buffer, size_t size) override
    {

        std::string payload((const char*)buffer, size);
        try{

            nlohmann::json msgContent = nlohmann::json::parse(payload);
            if(msgContent["MESSAGE_TYPE"] == 0) {
                std::cout << "Incoming (admin): " << payload << std::endl;
            }else{
                //process unicast message
                if(msgContent["PEER"] == m_username && m_destination == ""){
                    m_destination = msgContent["LOCAL"];
                    std::cout << "Connected to (" << m_destination << ")" << std::endl;
                    std::cout << "Incoming (" << m_destination << "): " << msgContent["MESSAGE"] << std::endl;
                }else{
                    std::cout << "Incoming (" << m_destination << "): " << msgContent["MESSAGE"] << std::endl;
                }
            }

        }catch (const char* message) { // Catches a throw with same data type
            std::cout << message << std::endl; // Outputs information about exception
        }
    }

    void onError(int error, const std::string& category, const std::string& message) override
    {
        std::cout << "PingPong TCP client caught an error with code " << error << " and category '" << category << "': " << message << std::endl;
    }

private:
    std::atomic<bool> _stop{false};
    std::string m_destination;
    std::string m_username;
};

int main(int argc, char** argv)
{
    // TCP server address
    std::string address = "127.0.0.1";
    if (argc > 1) address = argv[1];

    // TCP server port
    int port = 1111;
    if (argc > 2) port = std::atoi(argv[2]);

    //client's username
    std::string username = "client";
    srand(time(NULL));
    int iRand = rand() % 1000 + 1;
    username += std::to_string(iRand);
    if (argc > 3) username = argv[3];

    //destination's username
    std::string destination;
    if (argc > 4) destination = argv[3];

    std::cout << "TCP server address: " << address << std::endl;
    std::cout << "TCP server port: " << port << std::endl;
    std::cout << "Username: " << username << std::endl;
    std::cout << "Destination: " << destination << std::endl;

    std::cout << std::endl;

    // Create a new Asio service
    auto service = std::make_shared<AsioService>();

    // Start the Asio service
    std::cout << "Asio service starting...";
    service->Start();
    std::cout << "Done!" << std::endl;

    // Create a new TCP PingPong client
    auto client = std::make_shared<PingPongClient>(service, address, port);
    client->SetUsername(username);
    client->SetDestination(destination);

    // Connect the client
    std::cout << "Client connecting...";
    client->ConnectAsync();
    std::cout << "Done!" << std::endl;

    std::cout << "Press:";
    std::cout << "\n\t '#' to set the destination client";
    std::cout << "\n\tENTER to stop the client";
    std::cout << "\n\t'!' to reconnect to the server " << std::endl << std::endl;

    // Perform text input
    std::string line = "1";
    while (getline(std::cin, line))
    {
        if (line.empty()) break;

        // Reconnect the client
        if (line == "!")
        {
            std::cout << "Client reconnecting...";
            client->IsConnected() ? client->ReconnectAsync() : client->ConnectAsync();
            std::cout << "Done!" << std::endl;
            continue;
        }// Reconnect the client
        else if (line == "#")
        {
            std::cout << "Enter destination client username:\n";
            std::string dstUsername;
            getline(std::cin, dstUsername);
            client->SetDestination(dstUsername);
            std::cout << "Sending first message to " << dstUsername << "..." << std::endl;
            client->SendMessage("HELLO", 0);
            continue;

        }else{
            client->SendMessage(line);
        }
    }

    // Disconnect the client
    std::cout << "Client disconnecting...";
    client->DisconnectAndStop();
    std::cout << "Done!" << std::endl;

    // Stop the Asio service
    std::cout << "Asio service stopping...";
    service->Stop();
    std::cout << "Done!" << std::endl;

    return 0;
}

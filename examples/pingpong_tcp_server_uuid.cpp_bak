/*!
    \file pingpong_tcp_server.cpp
    \brief TCP pingpong server example
    University of Sarajevo
    Department of Telecommunications
    Software Design of Protocols
    www.tk.etf.unsa.ba
    \author Doc.dr Miralem Mehic dipl.ing.el. <miralem.mehic@etf.unsa.ba>
    \date 22.12.2021
    \copyright MIT License
*/

#include "asio_service.h"
#include "server/asio/tcp_server.h"

//https://github.com/d99kris/rapidcsv
//#include "server/rapidcsv/rapidcsv.h"

#include <iostream>
#include "server/json/json.h"

#include <sqlite3.h>
#include "server/sqlite3/sqlite3_wrapper.h"

#include <vector>
#include <iostream>

namespace sqlite = sqlite3_wrapper;
using namespace std;

class PingPongSession : public CppServer::Asio::TCPSession
{
public:
    using CppServer::Asio::TCPSession::TCPSession;

protected:
    void onConnected() override
    {
        std::string cip = socket().remote_endpoint().address().to_string();
        std::cout << "PingPong TCP session with Id " << id() << " (" << cip << ") is connected!" << std::endl;

        connectToDatabase();
        createDatabaseStructure();
        // Send invite message
        //std::string message("Hello from TCP PingPong! Please send a message or '!' to disconnect the client!");
        //SendAsync(message);
    }

    void onDisconnected() override
    {
        std::cout << "PingPong TCP session with Id " << id() << " disconnected!" << std::endl;
    }

    void onReceived(const void* buffer, size_t size) override
    {
        std::string payload((const char*)buffer, size);
        nlohmann::json msgContent = nlohmann::json::parse(payload);

        std::cout << "Incoming: " << payload << std::endl;
        std::cout << "Number of connected users is: " << server()->connected_sessions() << std::endl;


        std::cout << "uuid1:" << id() << std::endl;
        CppCommon::UUID uuid1(id().string());
        std::shared_ptr<TCPSession> peerSession1 = server()->FindSession( uuid1 );

        if(peerSession1 == nullptr){
            std::cout << "No client with id " << id() << " registered!" << std::endl;
        }else{
            std::cout << "Client with id " << id() << " found!" << std::endl;
        }

        if(msgContent["PEER"] != "" && msgContent["LOCAL"] != ""){

            CppCommon::UUID uuid(msgContent["PEER"]);

            std::cout << "uuid:" << uuid.string() << std::endl;
            std::shared_ptr<TCPSession> peerSession = server()->FindSession(uuid);

            if(peerSession == nullptr){
                std::cout << "No client with id " << msgContent["PEER"] << " registered!" << std::endl;
            }else{
                if(peerSession->IsConnected() == true){
                    peerSession->SendAsync(payload);
                }else{
                    std::cout << "Session with id " << msgContent["PEER"] << " is not connected!" << std::endl;
                }
            }
        }else{
            nlohmann::json msgBody;
            msgBody["PEER"] = msgContent["PEER"];
            msgBody["LOCAL"] = msgContent["LOCAL"];
            msgBody["MESSAGE"] = "New user registered!";
            std::string message = msgBody.dump();
            server()->Multicast(message);
        }

        // Multicast message to all connected sessions
        //std::string message2("Message Received!");
        //SendAsync(message2);

        // If the buffer starts with '!' the disconnect the current session
        if (msgContent["MESSAGE"] == "!")
            Disconnect();
    }

    void onError(int error, const std::string& category, const std::string& message) override
    {
        std::cout << "PingPong TCP session caught an error with code " << error << " and category '" << category << "': " << message << std::endl;
    }

private:

    int connectToDatabase()
    {
       /* Open database */
       int m_rc = sqlite3_open("pingpong.db", &db);

       if( m_rc ) {
          fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
          return(0);
       } else {
          fprintf(stderr, "Opened database successfully\n");
       }
       return 1;
    }

    void createDatabaseStructure()
    {

       std::string sql = "CREATE TABLE USERS (" \
          "ID INT PRIMARY KEY     NOT NULL," \
          "USERNAME       TEXT    NOT NULL," \
          "IP_ADDRESS     TEXT     NOT NULL," \
          "SESSION_ID     TEXT     NOT NULL);";

        int exit = 0;
        exit = sqlite3_open("example.db", &db);
        char* messaggeError;
        exit = sqlite3_exec(db, sql.c_str(), NULL, 0, &messaggeError);

        if (exit != SQLITE_OK) {
            std::cerr << "Error Create Table" << std::endl;
            sqlite3_free(messaggeError);
        } else {
            std::cout << "Table created Successfully" << std::endl;
        }
        sqlite3_close(db);
    }

    /*
    void insertNewUser(nlohmann::json msgContent){

       sql = "INSERT INTO USERS (ID,USERNAME,IP_ADDRESS,SESSION_ID) "  \
             "VALUES (1, 'Paul', 32, 'California', 20000.00 );";

       rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

       if( rc != SQLITE_OK ){
          fprintf(stderr, "SQL error: %s\n", zErrMsg);
          sqlite3_free(zErrMsg);
       } else {
          fprintf(stdout, "Records created successfully\n");
       }
    }
    */

    sqlite3 *db;
};






class PingPongServer : public CppServer::Asio::TCPServer
{
public:
    using CppServer::Asio::TCPServer::TCPServer;

protected:
    std::shared_ptr<CppServer::Asio::TCPSession> CreateSession(const std::shared_ptr<CppServer::Asio::TCPServer>& server) override
    {
        return std::make_shared<PingPongSession>(server);
    }

protected:
    void onError(int error, const std::string& category, const std::string& message) override
    {
        std::cout << "PingPong TCP server caught an error with code " << error << " and category '" << category << "': " << message << std::endl;
    }
    void onConnected(std::shared_ptr<CppServer::Asio::TCPSession>& session) override
    {
        std::cout << "PingPong TCP server is connected via session " << session->id() << std::endl;
    }
};





int main(int argc, char** argv)
{
    sqlite::db db("test.db");

    // TCP server port
    int port = 1111;
    if (argc > 1)
        port = std::atoi(argv[1]);

    std::cout << "TCP server port: " << port << std::endl;

    std::cout << std::endl;

    // Create a new Asio service
    auto service = std::make_shared<AsioService>();

    // Start the Asio service
    std::cout << "Asio service starting...";
    service->Start();
    std::cout << "Done!" << std::endl;

    // Create a new TCP PingPong server
    auto server = std::make_shared<PingPongServer>(service, port);

    // Start the server
    std::cout << "Server starting...";
    server->Start();
    std::cout << "Done!" << std::endl;

    std::cout << "Press Enter to stop the server or '!' to restart the server..." << std::endl;

    // Perform text input
    std::string line;
    while (getline(std::cin, line))
    {
        if (line.empty())
            break;

        // Restart the server
        if (line == "!")
        {
            std::cout << "Server restarting...";
            server->Restart();
            std::cout << "Done!" << std::endl;
            continue;
        }

        // Multicast admin message to all sessions
        line = "(admin) " + line;
        server->Multicast(line);
    }

    // Stop the server
    std::cout << "Server stopping...";
    server->Stop();
    std::cout << "Done!" << std::endl;

    // Stop the Asio service
    std::cout << "Asio service stopping...";
    service->Stop();
    std::cout << "Done!" << std::endl;

    return 0;
}
